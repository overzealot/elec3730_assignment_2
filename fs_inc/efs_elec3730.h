#ifndef __EFS_ELEC3730_H__
#define __EFS_ELEC3730_H__


/*****************************************************************************/
esint8 efsl_initStorage(efsl_storage *efsl_storage,efsl_storage_conf *config);
esint8 efsl_initFs(efsl_fs *efsl_filesystem,efsl_fs_conf *config);

/*****************************************************************************/
//These additions made by lecturer for ELEC3730
/*****************************************************************************/
typedef File EmbeddedFile;

typedef struct EmbeddedFileSystem{
	hwInterface myCard;
	IOManager myIOman;
	Disc myDisc;
	Partition myPart;
	FileSystem myFs;
};


esint8 efs_init(EmbeddedFileSystem * efs,eint8 * opts);
/*****************************************************************************/
//End of additions
/*****************************************************************************/

#endif
