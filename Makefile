CC=gcc
CFLAGS=-Wall -std=c99
INCLUDEDIRS=-Iefsl-0.2.8/inc -Iefsl-0.2.8/conf
LIBRARIES=-Lefsl-0.2.8 -Lefsl-0.2.8/src -L.
LIBS=-lefsl
#OBJ=efsl-0.2.8/libefsl.a
#OBJ=

all: clean assn2

assn2: assn2.c
	$(MAKE) -C efsl-0.2.8
	$(CC) $(CFLAGS) $(INCLUDEDIRS) $(LIBRARIES) assn2.c -lefsl -o assn2
#This is what I was working from, in case I forget.
#	gcc -Iefsl-0.2.8/inc -Iefsl-0.2.8/conf -Lefsl-0.2.8 -Lefsl-0.2.8/src efsl-0.2.8/libefsl.a assn2.c -lefsl -o assn2

clean: 
	rm -rf *.o assn2
	rm efsl-0.2.8/libefsl.a
